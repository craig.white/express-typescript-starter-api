import { App } from './app';
import { RecipeController } from './controllers';

const app = new App(
    [
    new RecipeController("/recipes"),
    ],
    3000
    );

app.initialise();