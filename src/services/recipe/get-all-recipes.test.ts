import { GetAllRecipes } from './get-all-recipes';
import { Recipe } from '../entity/recipe.entity';

jest.mock('../entity/recipe.entity');

describe('get-recipes-by-id', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    })
    it('exists', () => {
        expect(GetAllRecipes).toBeDefined();
    })
    it('uses the recipe entity', async () => {
        const mockResult = new Recipe();
        mockResult.name = "hi";
        const mockResultArray = [mockResult];
        const spyOn = jest.spyOn(Recipe, 'find').mockResolvedValue(mockResultArray)

        const result = await GetAllRecipes();

        expect(spyOn).toHaveBeenCalledTimes(1);
        expect(result).toEqual(mockResultArray);
    })
})