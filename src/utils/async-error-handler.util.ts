import { NextFunction, Request, Response } from "express";
import { sendErrorResponse } from './send-error.util';

type AsyncErrorHandler = (request: Request<any>, response: Response<any>, next: NextFunction) => Promise<Response<any>>

/**
 * Wraps an asynchronous controller method to catch thrown errors.
 * @example this.router.get(`${this.path}/:id`, asyncErrorHandler(this.getRecipeById));
 */
export const asyncErrorHandler = (handler: AsyncErrorHandler) => (req: Request<any>, res: Response<any>, next: NextFunction) => {
    handler(req, res, next)
		.catch(err => sendErrorResponse({
            request: req,
            response: res,
            message: err.message,
            status: err.status,
        }));
};
