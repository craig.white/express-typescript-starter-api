import { RecipeController } from './recipe.controller';

jest.mock('../../services/recipe/get-recipes-by-id');


describe("Controller for recipe items", () => {
    const Recipe = new RecipeController('/recipes');
    jest.mock('../../services')

    describe("getAllRecipes", () => {
        it('exists', () => {
            expect(Recipe.getAllRecipes).toBeDefined();
        })
    })

    describe('getRecipeById', () => {
        it('exists', () => {
            expect(Recipe.getRecipeById).toBeDefined();
        })
    })

    describe('initialiseRoutes', () => {
        it('exists', () => {
            expect(Recipe.initialiseRoutes).toBeDefined();
        })
    })
})