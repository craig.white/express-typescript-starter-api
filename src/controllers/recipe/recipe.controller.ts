import { Request, Response } from 'express';

import { BaseController } from '../base.controller';
import { 
    RecipeResponseDTO, 
    RecipeRequestDTO,
    createRecipeBodyValidation,
    getRecipeByIdParamValidation 
} from './dto';
import { GetRecipeById, CreateRecipe, GetAllRecipes } from '../../services';

import { StatusCodes } from '../../constants/status-codes';
import { asyncErrorHandler } from '../../utils/async-error-handler.util';
import { HttpError } from '../../utils/http-error.util';

export class RecipeController extends BaseController {
    
    public async initialiseRoutes(): Promise<void> 
    {
        this.router.get(this.path, asyncErrorHandler(this.getAllRecipes));
        this.router.get(`${this.path}/:id`, asyncErrorHandler(this.getRecipeById));
        this.router.post(this.path, asyncErrorHandler(this.createRecipe));
        this.router.get(this.path, asyncErrorHandler(this.getAllRecipes));
    }

    async getRecipeById(req: Request<RecipeRequestDTO>, res: Response<RecipeResponseDTO>): Promise<Response<RecipeResponseDTO>>
    {
            const { id }: {id: number} = await getRecipeByIdParamValidation.validateAsync(req.params);

            const response = await GetRecipeById(id);

            if (response instanceof Error) {
                throw new HttpError(
                    'Internal server error', 
                    StatusCodes.INTERNAL_SERVER_ERROR
                );
            }

            if (!response) {
                throw new HttpError('Recipe not found for supplied Id', StatusCodes.NOT_FOUND);
            }
            
            res.status(StatusCodes.OK);
            return res.send(response);
    }
    
    async getAllRecipes(req: Request, res: Response<RecipeResponseDTO[]>)
    {

        const response = await GetAllRecipes();

        if (response instanceof Error) {
            throw new HttpError(
                'Internal server error', 
                StatusCodes.INTERNAL_SERVER_ERROR
            );
        }

        if (!response) {
            throw new HttpError('Recipes not found in database', StatusCodes.NOT_FOUND);
        }
        
        res.status(StatusCodes.OK);
        return res.send(response);
    }

    async createRecipe(req: Request<RecipeRequestDTO>, res: Response)
    {

        console.log(req.body);

        const { name }: {name: string} = await createRecipeBodyValidation.validateAsync(req.body);

        const response = await CreateRecipe({
            name
        });

        if (response instanceof Error) {
            throw new HttpError(
                'Internal server error', 
                StatusCodes.INTERNAL_SERVER_ERROR
            );
        }

        res.status(StatusCodes.CREATED);
        return res.send({ id: response.identifiers[0].id });
    }

}