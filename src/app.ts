import express, { Express } from 'express';
import "reflect-metadata";
import bodyParser from 'body-parser';

import { BaseController } from './controllers/base.controller';

import { config } from './config';
import { loggerMiddleware } from './utils/middleware'; 
import { connectDb } from './db/connect.db';

export class App {
    public app: Express;
    public port: number;

    constructor(controllers: BaseController[], port: number) 
    {
        this.app = express();
        this.port = port;
        this.initialiseMiddleware();
        this.initialiseControllers(controllers);
        this.initialiseDb();
    }

    private initialiseControllers(controllers: BaseController[]) 
    {
        // Intialising each controller with correct route and logging middleware
        // to happen before every request.
        controllers.forEach((controller) => {
            this.app.use('/', loggerMiddleware, controller.router);
        });
    }

    private initialiseMiddleware() {
        this.app.use(bodyParser.urlencoded({ extended: false })); 
        this.app.use(bodyParser.json());
    }

    private initialiseDb() {
        connectDb();
    }

    async initialise(): Promise<void> 
    {
        this.app.listen(config.get('port'), () => {
            console.log('\x1b[42m\x1b[30m','APP', '\x1b[0m', `Listening on port ${config.get('port')}.`)
        })
    }

}
