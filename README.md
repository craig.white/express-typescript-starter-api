# express-typescript-starter-api
An approach for express-typescript projects, not perfect but always improving and open to suggestions.

Inspired by premise of [nest-js-api-starter](https://gitlab.com/joe.honour/nestjs-api-starter), in a pure Express format.

# installation
Once you have cloned the repo to your local machine simply run:

```bash 
$ yarn
$ yarn build:dev
```
For prod container:
```bash 
$ yarn build:docker
$ yarn run:compose:up
// OR
$ yarn run:docker
```

# contains
- Class based skeleton API setup with simple implementations
- Jest with coverage
- Supertest for testing HTTP
- ESLint & Prettier configured
- Husky with:
    - commitlint for consistent commits
    - pre-commit jest testing
- Controller validation
- Async & sync error handling with custom HTTP errors & status code enum.
- Convict env handling & validation for log levels
- Route Logging
- Dockerising the prod & dev builds into images.
    - Multi-stage with build, dep and main image.
    - Run on local with `yarn run:compose`.
- Postgres setup
- Basic Unit tests
    - Services
        - Existence and usage of typeORM.
    - Controller
        - Existence 
- Setup testcontainer
- Setup wiremock
- TODO: Integration tests
    - DB Healthcheck
    - Implementations
- TODO: E2E Tests
- TODO: Gitlab CI
    - Build
    - Test & lint
    - Docker
- TODO: Swagger documentation
- TODO: STRETCH: Use the contracts on an example React frontend.
- TODO: STRETCH: Improve tests
- TODO: STRETCH: Reduce image size (SPIKE)
- TODO: STRETCH: Use external API
- TODO: STRETCH: Use kubernetes
- TODO: STRETCH: Use prometheus and grafana
- TODO: STRETCH+: Connect to an API that isn't of this tech stack

# practices
- Import format [external, same-context, not-same-context]
- Alphabetical sorting of imports
- Referencing contract related types as DTO's, database related objects as entities.
- Context on filename (`file.context.ts`), all lower case or "kebab-case" if multiple words (`file-name.context.ts`) where it makes sense
- Frequent commits in [commitlint](https://commitlint.js.org/) format
- Referencing sources where solutions could have been inspired/taken from to enable transparency and understandability of solutions
- Business logic should be able to be re-used as a typical function, nothing connecting it to request/response apart from inputs requests MAY provide.

# structure
- controllers
- services
- utils
    - middleware
- constants
- config

# packages
| Dev Dependency (12) | Reason                                                                         |
|----------------|-------------------------------------------------------------------------------------|
| typescript     | for adding type-safety for javascript                                               |
| eslint         | code standards and code smells for use in CI, just using reccomended setup          |
| jest           | testing framework                                                                   |
| supertest      | http tests                                                                          |
| jest-junit     | coverage reports                                                                    |
| prettier       | code formatting                                                                     |
| husky          | pre-commit hooks, such as linting or checking commit formats before commits         |
| commitlint     | linting for consistent commit formats                                               |
| nodemon        | used for `yarn start:dev` mode to restart on file changes                           |
| ts-loader      | enables webpack for typescript compiling for optimised builds                       |
| eslint-config-prettier | eslint compatibility with prettier                                          |
| ts-node        | for nodemon typescript

| Dependency (7) | Reason                                                                              |
|----------------|-------------------------------------------------------------------------------------|
| express        | node framework for http servers                                                     |
| joi            | validation for express I/O                                                          |
| morgan         | logging middleware                                                                  |
| winston        | logger with levels                                                                  |
| convict        | env file schema and CLI arguments                                                   |
| reflect-metadata | typeorm dependency augmenting a class as it's defined                             |
| body-parser    | Used for parsing incoming HTTP request data, mainly in context of PUT, POST & PATCH |

Plan to use:

| Dependency     | Reason                                                                              |
|----------------|-------------------------------------------------------------------------------------|
| TSOA           | OpenAPI docs for a single source of truth for API functionality                     |

# notes
- Structure inspired by [docs link](https://wanago.io/2018/12/03/typescript-express-tutorial-routing-controllers-middleware/)
- Checking if thrown errors stay verbose can be checked by adding `process.env.NODE_ENV = 'production';` in a file.
- body-parser:
    - Body parser added for future use, as this generally pertains to POST requests, [what body-parser does](https://www.quora.com/What-exactly-does-body-parser-do-with-express-js-and-why-do-I-need-it.)
- convict:
    - Must specify `arg: NAME` in `config.ts` for it to be picked up as a cli argument, and then use as `--NAME <content>` when calling, and you access them via `config.get(<convict object key>)` rather than `process.env`
- docker:
    - `"build:docker": "docker build --rm -f docker/Dockerfile -t org.and/express-ts-api-starter:latest ."`
        - `-rm` => Remove after stop running container.
        - `-t` => Tag to identify image
    - `"run:docker": "docker run --rm org.and/express-ts-api-starter:latest"`
    - If you want to look inside a running container and `ls` around, run `docker exec -it <container> bash`, and then to see folder size use `du <directory> -h`, where `-h` means human readable and puts it in `mb/kb`.
- webpack:
    - Using webpack & `nodemon-webpack-plugin` with args relating to log levels, can have default as verbose when watching in development mode.
    - explored using webpack, only saved 90kb of space, but causes issues when looking towards using typeorm with the mappings to entity files etc, just creates too many dependencies.
- postgres: 
    - Postgres setup with typeORM: [link](https://wanago.io/2020/05/18/api-nestjs-postgresql-typeorm/).
    - [entity bug](https://github.com/typeorm/typeorm/issues/2897).

# footer

If you have any suggestions or comments throw them on this repo or send me an email via [my email](mailto:craig.white@and.digital).

Thanks